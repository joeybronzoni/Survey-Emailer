/* Reffered to commonJS modules- a system implemented in nodejs for requiring/sharing code
between different files, node does not have support for ES6 modules but on the front end we will use import-from syntax*/
// Lib inports
const express = require('express');
const app = express();

// Local imports
const PORT = process.env.PORT || 5000;

// route handlers
app.get('/', (req,res) => {
  res.send({bye: 'there'});
});

const server = app.listen(PORT, () => {
  console.log(`Express running on PORT, ${PORT}`);
});
